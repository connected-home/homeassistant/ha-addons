#!/bin/bash
IP=$(ip route get 10.64.0.1 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}')
URL=$(</data/url)
LOGIN=$(</data/login)

curl    -H "Accept: application/json" \
        -H "Authorization: Bearer $HASSIO_TOKEN" \
        --request POST \
        -s --data "{\"state\": \"online\",\"attributes\":{\"url\": \"https://$URL\",\"ip\" : \"$IP\",\"login\" : \"$LOGIN\"}}" http://supervisor/core/api/states/sensor.CHCloud_client_state > /dev/null

