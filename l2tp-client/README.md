# Connected Home Add-on: CHCloudClient

L2TP client for Connected Home Cloud

![Supports amd64 Architecture][amd64-shield] ![Supports armhf Architecture][armhf-shield] ![Supports armv7 Architecture][armv7-shield] ![Supports i386 Architecture][i386-shield]

## About
You can use this add-on to create l2tp tunnel to CH cloud server. This add-on provides access to homeassistant from internet.
[mosquitto].

## Installation

Follow these steps to get the add-on installed on your system:

1. Navigate in your Home Assistant frontend to **Hass.io** -> **Add-on Store**.
2. Add new repository by URL https://gitlab.com/connected-home/ha-addons.git
2. Find the "CHCloudClient" add-on and click it.
3. Click on the "INSTALL" button.

## How to use

The add-on has a couple of options available. To get the add-on running:

1. Change default configuration settings with your server and login
2. Start the add-on.
3. Check the add-on log output to see the result.
4. Check the status of sensor "CHCloud_client_state"

## Configuration

Add-on configuration:

```json
{
  "server": "home.bi.group",
  "login": "hq@cw.home.bi.group",
  "debug": "no"
}
```

### Option: `server`

Domain or IP address of CHCloud server. Optionally you can provide port number after colon. Default port is 5001

### Option: `login`

Unique login in e-mail format. Used as domain addres to external access

### Option: `debug`

Change to "yes" to verbose logging. Default value "no"

[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
[armhf-shield]: https://img.shields.io/badge/armhf-yes-green.svg
[armv7-shield]: https://img.shields.io/badge/armv7-yes-green.svg
[discord]: https://discord.gg/c5DvZ4e
[forum]: https://community.home-assistant.io
[i386-shield]: https://img.shields.io/badge/i386-yes-green.svg
[issue]: https://github.com/home-assistant/hassio-addons/issues
[reddit]: https://reddit.com/r/homeassistant
[repository]: https://github.com/hassio-addons/repository
[mosquitto]: https://mosquitto.org/
