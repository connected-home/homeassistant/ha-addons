#!/bin/bash
#FILE=/data/salt
CONFIG_PATH=/data/options.json
DEFAULT_TIMEOUT=$(jq --raw-output ".timeout" "$CONFIG_PATH")
if [[ "$DEFAULT_TIMEOUT" == "null" ]]; then
    DEFAULT_TIMEOUT=30
fi
_term() {
  echo "Caught SIGTERM signal!"
  /stop.sh
  kill -TERM "$CHILD" 2>/dev/null
  exit 0
}
trap _term SIGTERM
#sleep 5
/start.sh
echo "Starting xl2tp daemon...";
L2TP_PORT=$((5000+${RANDOM:1:1}))
/run-l2tp.sh $L2TP_PORT &
echo "Daemon started"
CHILD=$!
#wait "$child"
INTERVAL=5
LOST_TIME=0
TIMEOUT=$DEFAULT_TIMEOUT
while true 
do
	
	if ! kill -n 0 "$CHILD" > /dev/null 2>&1
    then
		echo "xl2tpd is not running. Starting..."
		L2TP_PORT=$((5000+${RANDOM:1:1}))
		/run-l2tp.sh $L2TP_PORT &
		CHILD=$!
	fi
	
	if ping -c 1 "$L2TP_SERVER_IP" > /dev/null 2>&1
    then
		/up.sh
		LOST_TIME=0
		TIMEOUT=$DEFAULT_TIMEOUT
	else
		/down.sh
		((LOST_TIME=LOST_TIME+INTERVAL))
		if [[ $LOST_TIME -ge $TIMEOUT ]]
		then
			echo "No ping to server for $TIMEOUT seconds. Killing ..."
			kill -n 15 "$CHILD" > /dev/null
			LOST_TIME=0
			((TIMEOUT=2*TIMEOUT))
		fi
    fi
	sleep 5
done
