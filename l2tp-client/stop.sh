#!/bin/bash

curl 	--header "Content-Type: application/json" \
	--header "Authorization: Bearer $HASSIO_TOKEN" \
	--request POST \
  	-s --data "{\"state\": \"stopped\"}" http://supervisor/core/api/states/sensor.CHCloud_client_state > /dev/null

