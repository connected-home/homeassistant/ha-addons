#! /usr/bin/env bash
set -eu
L2TP_PORT="$1"
CONFIG_PATH=/data/options.json
LOGIN=$(jq --raw-output ".login" "$CONFIG_PATH")
SERVER=$(jq --raw-output ".server" "$CONFIG_PATH")
DEBUG=$(jq --raw-output ".debug" "$CONFIG_PATH")
TX_BPS=$(jq --raw-output ".tx_bps" "$CONFIG_PATH")
RX_BPS=$(jq --raw-output ".rx_bps" "$CONFIG_PATH")
PASSWORD="smarthome"
#USE_SALT="${USE_SALT,,}" # convert to lowercase
USER=${LOGIN%@*}
DOMAIN=${LOGIN#*@}
#/usr/bin/head -c 100 /dev/urandom | /usr/bin/tr -dc "a-zA-Z0-9" | /usr/bin/fold -w 12 | /usr/bin/head -n 1 > /user-id
URL="$USER.$DOMAIN"
printf "%s" "$URL" > /data/url
printf "%s" "$LOGIN" > /data/login
if [[ "$DEBUG" == "null" ]]; then
    DEBUG="no"
fi
if [[ "$SERVER" == "null" ]]; then
    SERVER="$DOMAIN"
fi
if [[ "$TX_BPS" == "null" ]]; then
    TX_BPS=10000000
fi
if [[ "$RX_BPS" == "null" ]]; then
    RX_BPS=10000000
fi
mkdir -p /etc/xl2tpd
cat > /etc/xl2tpd/xl2tpd.conf <<EOF
[global]
access control = no
auth file = /etc/ppp/pap-secrets
debug avp = $DEBUG
debug network = $DEBUG
debug packet = $DEBUG
debug state = $DEBUG
debug tunnel = $DEBUG
port = $L2TP_PORT

[lac home]
lns = $SERVER
redial = yes
redial timeout = 5
max redials = 65536
require chap = no
require authentication = no
ppp debug = $DEBUG
pppoptfile = /etc/ppp/options.l2tpd
require pap = yes
autodial = yes
tx bps = $TX_BPS
rx bps = $RX_BPS
length bit = yes
EOF

mkdir -p /etc/ppp
cat > /etc/ppp/options.l2tpd <<EOF
ipcp-accept-local
ipcp-accept-remote
refuse-eap
noipdefault
#require-mschap-v2
noccp
noauth
noaccomp
nopersist
defaultroute
usepeerdns
debug
maxfail 10
mtu 1400
mru 1400
name smarthome
user $URL
password $PASSWORD
EOF
mkdir -p /var/run/xl2tpd
rm -f /var/run/xl2tpd.pid
exec /usr/local/sbin/xl2tpd -D -c /etc/xl2tpd/xl2tpd.conf
